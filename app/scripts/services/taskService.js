'use strict';

angular.module('warsjawaApp')
    .service('taskService', function taskService($http, $resource) {
      var URL = "http://localhost\\:8080";

      var Task = $resource(URL);

      return {
        add: function (toAdd) {
          var task = new Task(toAdd);
          task.$save();
          return task;
        },
        list: function () {
          return Task.query();
        }
      };
    });
