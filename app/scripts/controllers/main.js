'use strict';

angular.module('warsjawaApp')
    .controller('MainCtrl', function ($scope, taskService) {

      $scope.tasks = [];

      $scope.add = function () {
        var toAdd = {name: $scope.newTask};
        $scope.tasks.push(taskService.add(toAdd));
        $scope.newTask = "";
      };

      $scope.isEmpty = function () {
        return !$scope.newTask;
      };

      $scope.stateChanged = function(toChange) {
        if (toChange.done) {
          toChange.finishedOn = new Date();
        }else{
          delete toChange.finishedOn;
        }
        taskService.add(toChange);
      };

      $scope.list = function(){
        $scope.tasks = taskService.list();
      }

    });
