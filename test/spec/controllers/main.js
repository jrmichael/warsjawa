'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('warsjawaApp'));

  var MainCtrl,
      scope, taskService;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, _taskService_) {
    scope = $rootScope.$new();
    taskService = _taskService_;
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));


  it("should add a new task", function () {
    var taskName = "some task";
    var task = {name: taskName, id: 1};
    spyOn(taskService, 'add').andReturn(task);
    scope.newTask = taskName;

    scope.add();

    expect(scope.tasks).toContain(task);
  });

  it("should cleanup after adding task", function () {
    scope.newTask = "some task";
    spyOn(taskService, 'add');

    scope.add();

    expect(scope.newTask).toEqual("");
  });

  it("should be empty", function () {
    scope.newTask = "";

    expect(scope.isEmpty()).toBeTruthy();
  });


  it("should set completion date", function () {
    var task = {name: "some task", done: true};
    spyOn(taskService, 'add');

    scope.stateChanged(task);

    expect(task.finishedOn).toBeDate();
  });

  it("should remove completion date", function () {
    var task = {name: "some task", done: false};
    spyOn(taskService, 'add');

    scope.stateChanged(task);

    expect(task.finishedOn).toBeUndefined();
  });

  it("state changing should persist", function () {
    var task = {name: "some task", done: false};
    spyOn(taskService, 'add');

    scope.stateChanged(task);

    expect(taskService.add).toHaveBeenCalledWith(task);

  });

  it("should list tasks from backend", function () {

    var tasks = [
      {id: 1},
      {id: 2}
    ];
    spyOn(taskService, 'list').andReturn(tasks);


    scope.list();

    expect(scope.tasks).toEqual(tasks);

  });
});
