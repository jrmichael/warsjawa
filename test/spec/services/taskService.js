'use strict';

describe('Service: taskService', function () {

  // load the service's module
  beforeEach(module('warsjawaApp'));

  var URL = "http://localhost:8080";

  // instantiate service
  var taskService, http;
  beforeEach(inject(function (_taskService_, $httpBackend) {
    taskService = _taskService_;
    http = $httpBackend;
  }));

  it('should add', function () {
    var task = {name: "sample"};
    var response = {id: 1, name: "sample"};
    http.expectPOST(URL, task).respond(200, response);

    var added = taskService.add(task);
    http.flush();

    expect(added.id).toEqual(response.id);

  });

  it("should get list", function () {
    http.expectGET(URL).respond(200, [{id:1},{id:2}]);

    var list = taskService.list();
    http.flush();

    expect(list.length).toBe(2);
  });

});
