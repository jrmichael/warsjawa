This application was developed during 2013 [warsjawa](http://warsjawa.pl/) workshop.
 
In order to run it you need our [rest server](https://bitbucket.org/jrmichael/rest-server) running and nodejs installed

Building:
```
npm install
bower install
```

bower and grunt might want to be installed globally:
```
npm install -g bower
npm install -g grunt
```

running: `grunt server`
